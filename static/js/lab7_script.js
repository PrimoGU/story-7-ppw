function accordion(){
    var acc = document.getElementsByClassName("accord");
    var i;
    for (i = 0; i < acc.length; i++) {
        acc[i].addEventListener("click", function() {
            this.classList.toggle("active");
            var panel = this.nextElementSibling;
            if (panel.style.maxHeight) {
                panel.style.maxHeight = null;
            } else {
                panel.style.maxHeight = panel.scrollHeight + "px";
            } 
        });
    }
}

function change(){
    var bodi = document.getElementsByTagName('body')[0]
    console.log(bodi.style.backgroundColor)
    if(bodi.style.backgroundColor == "rgb(255, 255, 255)"){
        bodi.style.backgroundColor = "rgb(49, 49, 49)";
        bodi.style.color = 'white';
    } else {
        bodi.style.backgroundColor = "rgb(255, 255, 255)";
        bodi.style.color = "black";
    }
} 
