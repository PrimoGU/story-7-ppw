from django.urls import path, include
from . import views

app_name = 'lab7'

urlpatterns = [
    path('', views.lab7_view, name='view_lab7'),
]
